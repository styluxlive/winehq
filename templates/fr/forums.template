<!--TITLE:[Listes de diffusion / Forums]-->
<!--BLURB:[Ce que nous voulons éviter, c'est un problème de communication.]-->

<h1 class="title">Listes de diffusion principales / Forums</h1>

<p>Wine propose divers listes de diffusion et forums.
Voici les plus utiles pour les utilisateurs :

<ul>
    <li>Les <a href="//forums.winehq.org">forums WineHQ</a></li>

    <li>Les utilisateurs Ubuntu peuvent également visiter l'<a href="http://ubuntuforums.org/forumdisplay.php?f=313">espace Wine des forums Ubuntu</a>

    <li>Si vous voulez être prévenu(e) des nouvelles versions, inscrivez-vous à la <a href="//www.winehq.org/mailman/listinfo/wine-announce">liste de diffusion Wine-Announce</a></li>
</ul>

<p>Merci de ne pas créer de forum non-anglophone pour Wine (à part éventuellement un espace Wine dans le forum principal d'une distribution populaire).</p>

<p>Si vous connaissez un autre forum Wine actif (en particulier un non-anglophone), merci de nous prévenir afin de pouvoir l'ajouter à la liste (un courriel à dank@kegel.com suffit).</p>

<h2>Tous les listes WineHQ</h2>

<p>WineHQ dispose de listes pour soumettre des patchs, suivre des commits Git, et discuter sur Wine. Elles permettent une configuration fine des inscriptions des utilisateurs, <a href="https://www.winehq.org/mailman3/postorius/lists/?all-lists">via le Web</a>.</p>

<p><b>Note :</b> il est nécessaire d'être inscrit pour pouvoir poster, ou votre message sera considéré comme unpourriel potentiel par le logiciel de gestion de la liste.</p>

<ul class="roomy">

  <li>
    <b><a href="mailto:wine-announce@winehq.org">wine-announce@winehq.org</a></b><br>
    [<a href="https://www.winehq.org/mailman3/postorius/lists/wine-announce.winehq.org/">(Dés-)inscription</a>]
    [<a href="https://www.winehq.org/mailman3/hyperkitty/list/wine-announce@winehq.org/">Archive</a>]
    Une liste bas trafic (2 messages par mois) en lecture seule annonçant les nouvelles versions et d'autres nouvelles majeures sur Wine ou WineHQ.
  </li>

  <li>
    <b><a href="mailto:wine-devel@winehq.org">wine-devel@winehq.org</a></b><br>
    [<a href="https://www.winehq.org/mailman3/postorius/lists/wine-devel.winehq.org/">(Dés-)inscription</a>]
    [<a href="https://www.winehq.org/mailman3/hyperkitty/list/wine-devel@winehq.org/">Archive</a>]
    Une liste moyen trafic (50 messages par jour) pour la discussion sur le développement de Wine, les patchs ou tout autre élément intéressant pour les développeurs Wine.
  </li>

  <li>
    <b><a href="mailto:wine-commits@winehq.org">wine-commits@winehq.org</a></b><br>
    [<a href="https://www.winehq.org/mailman3/postorius/lists/wine-commits.winehq.org/">(Dés-)inscription</a>]
    [<a href="https://www.winehq.org/mailman3/hyperkitty/list/wine-commits@winehq.org/">Archive</a>]
    Une liste moyen trafic (25 messages par jour) en lecture seule pour le suivi des commits dans Git.
  </li>

  <li>
    <b><a href="mailto:wine-releases@winehq.org">wine-releases@winehq.org</a></b><br>
    [<a href="https://www.winehq.org/mailman3/postorius/lists/wine-releases.winehq.org/">(Dés-)inscription</a>]
    [<a href="https://www.winehq.org/mailman3/hyperkitty/list/wine-releases@winehq.org/">Archive</a>]
    Une liste bas trafic (2 messages par mois) en lecture seule pour la réception de fichiers diff conséquents lors de la sortie d'une version officielle de Wine.
  </li>

  <li>
    <b><a href="mailto:wine-bugs@winehq.org">wine-bugs@winehq.org</a></b><br>
    [<a href="https://www.winehq.org/mailman3/postorius/lists/wine-bugs.winehq.org/">(Dés-)inscription</a>]
    [<a href="https://www.winehq.org/mailman3/hyperkitty/list/wine-bugs@winehq.org/">Archive</a>]
    Une liste haut trafic (100 messages par jour) en lecture seule pour la surveillances des activités sur la
    <a href="//bugs.winehq.org/">base de données de suivi de bogues</a>.
  </li>

</ul>
