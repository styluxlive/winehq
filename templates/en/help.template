<!--TITLE:[Getting Help]-->

<h1 class="title">Getting Help</h1>

<p>There are a variety of ways to get help in using Wine.</p>

<div class="black inverse bold cornerround padding-sm">
    <div class="row">
        <div class="col-xs-2 col-md-1">Option</div>
        <div class="col-xs-10 col-md-11">About this Help Option</div>
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="http://www.codeweavers.com"><i class="fas fa-money-bill-alt fa-2x"></i><br>Paid</a>
    </div>
    <div class="col-xs-10 col-md-11">
        <a href="http://www.codeweavers.com">CodeWeavers</a> offers paid support for Wine.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="https://wiki.winehq.org/FAQ"><i class="fas fa-info-circle fa-2x"></i><br>FAQ</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Most questions can be quickly answered by browsing our
        <a href="https://wiki.winehq.org/FAQ">FAQ</a>.
        This is the first place you should check to see if your question has been answered.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="{$root}/documentation"><i class="fas fa-folder-open fa-2x"></i><br>Docs</a>
    </div>
    <div class="col-xs-10 col-md-11">
        We recommend that you read our online <a href="{$root}/documentation">Documentation</a>
        to find the answer to your question. Also of interest is our
        <a href="https://wiki.winehq.org/Wine_Installation_and_Configuration">Wine Installation and Configuration</a> wiki page, which contains some useful information for
        the first time user.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="https://wiki.winehq.org/"><i class="fas fa-book fa-2x"></i><br>Wiki</a>
    </div>
    <div class="col-xs-10 col-md-11">
        You can also see our <a href="https://wiki.winehq.org/">Wiki</a> to
        see if you can find an answer to your question.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="//forums.winehq.org/"><i class="fas fa-comments fa-2x"></i><br>Forum</a>
    </div>
    <div class="col-xs-10 col-md-11">
        We have a <a href="//forums.winehq.org/">web</a>/<a href="{$root}/forums">email</a> based community for
        Wine users, which is a place that Wine users can visit to ask questions and connect
        with each other.
        You may also wish to explore the <a href="{$root}/forums">mailing lists</a>
        that make up that community as well.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="{$root}/irc"><i class="fas fa-comment-alt fa-2x"></i><br>IRC</a>
    </div>
    <div class="col-xs-10 col-md-11">
        You can connect with the Wine community via live chat
        on our <a href="{$root}/irc">IRC channel</a>.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="//appdb.winehq.org/"><i class="fas fa-database fa-2x"></i><br>AppDB</a>
    </div>
    <div class="col-xs-10 col-md-11">
        If you want help with a single application our <a href="//appdb.winehq.org/">Application Database</a>
        is a good place to look.
    </div>
</div>

<div class="row padding-md">
    <div class="col-xs-2 col-md-1 center">
        <a href="//bugs.winehq.org/"><i class="fas fa-bug fa-2x"></i><br>Bugzilla</a>
    </div>
    <div class="col-xs-10 col-md-11">
        Our <a href="//bugs.winehq.org/">Bugzilla Bug Tracker</a> contains all the issues we have
        worked on and are currently working on.
    </div>
</div>

<p>Problems with our website or notice a typo? Contact our <a href="mailto:web-admin@winehq.org">Web Team</a>.</p>
