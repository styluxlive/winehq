<!--TITLE:[Über Wine]-->

<h1 class="title">Über Wine</h1>

<p>Wine (ursprünglich ein Akronym für "Wine Is Not an Emulator") ist eine
Kompatibilitätsschicht, die es ermöglicht, Windows-Anwendungen unter
POSIX-konformen Betriebssystemen auszuführen, wie z.B. Linux,
macOS und BSD. Statt interne Windows-Logik zu simulieren, wie eine virtuelle
Maschine oder ein Emulator, übersetzt Wine die Windows-API-Aufrufe in
Echtzeit zu entsprechenden POSIX-Aufrufen und eliminiert somit die
Performance- und Speichereinbußen, die andere Methoden nach sich ziehen.
Wine erlaubt es auf diese Weise, Windows-Anwendungen sauber in Ihre
Desktopumgebung zu integrieren.</p>

<p>Das Projekt begann 1993, unter der anfänglichen Koordinierung durch Bob Amstadt,
eine Möglichkeit zu entwickeln, Windows 3.1-Programme unter Linux auszuführen.
Schon recht früh übernahm Alexandre Julliard die Projektleitung, welcher die
Wine-Entwicklung bis heute anführt. Über die Jahre haben sich die Windows-API
und Anwendungen weiterentwickelt, um Möglichkeiten neuer Hard- und Software
zu nutzen. Wine wurde fortwährend angepasst, um neue Funktionen zu unterstützen,
auf anderen Betriebsystemen lauffähig zu sein, stabiler zu werden und ein besseres
Nutzungserlebnis zu bieten.</p>

<p>Obgleich es per Definition bereits ein sehr ambitioniertes Projekt war,
schritt die Entwicklung von Wine im Laufe von 15 Jahren stetig voran, um im Jahre 2008
endlich Version 1.0 zu erreichen, das erste stabile Release. Einige Jahre später
befindet sich Wine heute weiterhin unter aktiver Entwicklung. Auch wenn es noch einiges
zu tun gibt, benutzen schätzungsweise bereits 
<a href="https://wiki.winehq.org/UsageStatistics" title="Wine Usage
Statistics">Millionen von Menschen</a> Wine, um Windows-Software auf dem Betriebsystem
ihrer Wahl auszuführen.</p>

<h3>Open-Source und gelenkt durch seine Benutzer</h3>

<p>Wine wird immer <a href="https://wiki.winehq.org/Licensing"
title="Wine licensing">freie Software</a> sein. Ungefähr die Hälfte des
Quelltextes stammt von freiwilligen Entwicklern, während die übrige Arbeit durch
kommerzielle Interessen erbracht wird, insbesondere von
<a href="http://www.codeweavers.com/products/" title="CodeWeavers
CrossOver Office">CodeWeavers</a>, welche eine bezahlte Version von Wine mit
professionellem Support anbieten.</p>

<p>Wine ist sehr auf seine Nutzergemeinschaft angewiesen. Anwender verbringen ihre
Freizeit damit, Tipps und Testergebnisse zu teilen, um in unserer
<a href="//appdb.winehq.org/" title="Wine Application
Database">Applikations-Datenbank</a> zu dokumentieren, wie gut ihre Software läuft,
Fehlerberichte in unserem <a href="//bugs.winehq.org/"title="Bugzilla">Bugtracker</a>
zu erstellen, um Entwickler auf Probleme hinzuweisen und Fragen in unseren
<a href="//forums.winehq.org/" title="WineHQ Forums">Foren</a> zu beantworten.
</p>

<h3>Erfahren Sie mehr:</h3>

<p><i>(Etliche der folgenden Seiten werden momentan überarbeitet und
sind daher unter Umständen nicht aktuell)</i></p>

<ul>
    <li><a href="https://wiki.winehq.org/ImportanceOfWine">Warum Wine so
        wichtig ist (en)</a>
    </li>
    <li><a href="https://wiki.winehq.org/Debunking_Wine_Myths">Häufige
        Mythen über Wine aufgedeckt (en)</a>
    </li>
    <li><a href="https://wiki.winehq.org/WineHistory">Geschichte des
        Wine-Projekts (en)</a>
    </li>
    <li><a href="https://wiki.winehq.org/ProjectOrganization">
        Organisation und Leitung (en)</a>
    </li>
    <li><a href="https://wiki.winehq.org/WhosWho">Kurzbiografien einiger
        Beitragenden (en)</a>
    </li>
    <li><a href="https://wiki.winehq.org/Acknowledgements">Andere
        Anerkennungen (en)</a>
    </li>
</ul>
