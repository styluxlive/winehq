<?xml version="1.0" ?>
<kc>
<title>Wine Traffic</title>

<author contact="http://www.dawncrow.de/wine/">Andr&#233; Hentschel</author>
<issue num="400" date="10/04/2015" />
<intro> <p>This is the 400th issue of the World Wine News publication.
Its main goal is to inform you of what's going on around Wine. Wine is an open source implementation of the Windows API on top of X and Unix.  Think of it as a Windows compatibility layer.  Wine does not require Microsoft Windows, as it is a completely alternative implementation consisting of 100% Microsoft-free code, but it can optionally use native system DLLs if they are available.   You can find more info at <a href="http://www.winehq.org">www.winehq.org</a></p> </intro>
<stats posts="180" size="162" contrib="42" multiples="21" lastweek="21">

<person posts="6" size="7" who="piotr.caban at gmail.com (Piotr Caban)" />
<person posts="5" size="14" who="sebastian at fds-team.de (Sebastian Lackner)" />
<person posts="5" size="11" who="ken at codeweavers.com (Ken Thomases)" />
<person posts="4" size="8" who="stefandoesinger at gmail.com (Stefan Dösinger)" />
<person posts="4" size="7" who="jem456.vasishta at gmail.com (Aaryaman Vasishta)" />
<person posts="4" size="7" who="austinenglish at gmail.com (Austin English)" />
<person posts="4" size="6" who="bunglehead at gmail.com (Nikolay Sivov)" />
<person posts="4" size="5" who="dmitry at baikal.ru (Dmitry Timoshkov)" />
<person posts="3" size="6" who="jactry92 at gmail.com (Jactry Zeng)" />
<person posts="3" size="4" who="hverbeet at gmail.com (Henri Verbeet)" />

</stats>

<section
	title="WineConf 2015"
	subject="WineConf"
	archive="https://wiki.winehq.org/WineConf2015"
	posts="3"
>
<p>
    As you might already have heard we want to integrate wine-staging into the Wine
    development process. Here is the official mail which also includes other changes.
</p>
<![CDATA[
<p>
    Alexandre Julliard wrote in:
</p>
<blockquote>
<pre>
Over the last few months there have been a lot of discussions about how
to improve our development process. I've been gathering feedback, and
last week at WineConf I summarized the suggestions in my keynote
presentation; the slides can be viewed at
https://wiki.winehq.org/WineConf2015?action=AttachFile&do=get&target=wineconf-2015.pdf

We've then had a lot of constructive discussions about the various
points. The major decisions we've agreed on are:

- Wine-staging is now considered an integral part of the Wine
  development process, and will be used as a mechanism to enable more
  patches to meet the requirements for inclusion in the main tree. We
  will all be working together as a team.

- Bugs reported against wine-staging will be accepted in the WineHQ bug
  tracker; there will be a way to distinguish bugs specific to Staging,
  and bugs that are fixed in Staging but not yet in main Wine. The
  Staging bug tracker will be retired. Austin English is in charge of
  implementing the necessary changes in Bugzilla.

- We will switch to a time-based stable release, on a yearly
  schedule. The code freeze will start every year in the fall. Michael
  Stefaniuc will be maintaining the stable branch starting with 1.8.

- We will start enforcing a Signed-Off-By header on patches, to make it
  possible to better distribute reviewing responsibility, and to allow
  multiple authors to cooperate on a patch.

- We will keep a list of maintainer contact information for the various
  submodules; developers will be encouraged to go through the respective
  maintainer before submitting to wine-patches.

- There will be a group of people who volunteer to be assigned patches
  to review, to make sure that no patch goes unreviewed. Going through
  Staging first will also be encouraged for unfinished or risky patches.

- The patch tracker will send automated emails when a patch status
  changes; this will also serve to encourage discussion rather than
  despair when a patch is not approved.

- We will start building and distributing binary packages for all
  distros that don't have readily available packages. The packaging
  scripts and control files will be maintained in git, so that people
  can review them and submit improvements.

These changes will be implemented over the next few weeks.

I'm hoping that this will make the development process more pleasant for
everybody, and enable us to better respond to users' needs.

Once these changes are in place, I'll also encourage everybody who had
given up in disgust to give us another chance; and if things are still
not satisfactory, please send us feedback. This is a work in progress,
and we will continue to listen and work on making things better.
</pre>
</blockquote>
]]>
<p>
    One of the steps to achive the integration is to adjust our Bugzilla settings.
</p>
<![CDATA[
<p>
    Austin English wrote in:
</p>
<blockquote>
<pre>
Howdy all,

As Alexandre mentioned [1], at WineConf we made several decisions to
modify bugzilla in a few ways. I've now implemented those changes,
which are outlined below:

* New wine-staging product: This should be used for bugs caused by
patches that are in wine-staging, but that do not occur in
wine-development (i.e., wine-staging patch regressions)

* New STAGED resolution: This is to differentiate bugs that are FIXED
(in wine-development) from bugs that are not present in wine-staging
because of one or more patches. The anticipated workflow is:
UNCONFIRMED > bug confirmed, NEW > patch written and sent to
wine-patches, if it's accepted, FIXED. If not, and the patch is
integrated into wine-staging, then the bug is STAGED. When the patch
is revised and eventually integrated into wine-development, the bug
should move to FIXED.

* New NEEDINFO resolution: There's a lot of confusion and different
handling by triagers for what to do with bug reports that are
incomplete (i.e., leaving it open versus closing invalid). To mitigate
this, I've added a NEEDINFO resolution. If a bug report lacks needed
information, set it to this status. Bug that have been open NEEDINFO
for more than 1 year can be closed.

* Renamed UPSTREAM to NOTOURBUG: This is more in line with what other
projects do, and eliminates confusion about the upstream/downstream
distinction.
</pre>
</blockquote>
]]>
<p>
    For now some things are still not implemented,
    for example a patch flow for staging-only patches,
    patch tracker adjustments/improvements,
    providing binary packages and maybe a Git mirror of the staging tree at WineHQ
</p>
<p>
    In the meantime you might want to view the Videos of some presentations:
</p>
<ul>
<li><a href="https://www.youtube.com/watch?v=GTN5ksxzFnE">Keynote presentation – Alexandre Julliard</a></li>
<li><a href="https://www.youtube.com/watch?v=EzR9RPlWi7c">Brief update on CodeWeavers - Jeremy White</a></li>
<li><a href="https://www.youtube.com/watch?v=QdcFNqWTja4">Wine Staging Introduction - Michael Stefaniuc</a></li>
</ul>
<p>
    To be continued...
</p>
</section>

<section
	title="Monetary value of Wine"
	subject=""
	archive="//www.winehq.org/pipermail/wine-devel/2015-October/109507.html"
	posts="1"
>
<p>
    Tom Wickline wrote an article about figuring out the monetary value of Wine:
</p>
<p>
    <a href="http://www.wine-reviews.net/2015/10/the-glass-of-wine-thats-worth-350.html">The glass of wine thats worth 350 million dollars</a>
</p>
</section>

<section
    title="Weekly AppDB/Bugzilla Status Changes"
    subject="AppDB/Bugzilla"
    archive="//appdb.winehq.org"
    posts="0"
>
<topic>AppDB / Bugzilla</topic>
<center><b>Bugzilla Changes:</b></center>
<p><center>
<table border="1" bordercolor="#222222" cellspacing="0" cellpadding="3">
  <tr><td align="center">
        <b>Category</b>
  </td><td>
         <b>Total Bugs Last Issue</b>
  </td><td>
        <b>Total Bugs This Issue</b>
  </td><td>
        <b>Net Change</b>
  </td></tr>  <tr>
    <td align="center">
     UNCONFIRMED
    </td>
    <td align="center">
     2600
    </td>
    <td align="center">
     2577
    </td>
    <td align="center">
      -23
    </td>
  </tr>  <tr>
    <td align="center">
     NEW
    </td>
    <td align="center">
     3424
    </td>
    <td align="center">
     3283
    </td>
    <td align="center">
      -141
    </td>
  </tr>  <tr>
    <td align="center">
     ASSIGNED
    </td>
    <td align="center">
     17
    </td>
    <td align="center">
     16
    </td>
    <td align="center">
      -1
    </td>
  </tr>  <tr>
    <td align="center">
     REOPENED
    </td>
    <td align="center">
     113
    </td>
    <td align="center">
     113
    </td>
    <td align="center">
      0
    </td>
  </tr>  <tr>
    <td align="center">
     RESOLVED
    </td>
    <td align="center">
     214
    </td>
    <td align="center">
     230
    </td>
    <td align="center">
      +16
    </td>
  </tr>  <tr>
    <td align="center">
     CLOSED
    </td>
    <td align="center">
     32887
    </td>
    <td align="center">
     32988
    </td>
    <td align="center">
      +101
    </td>
  </tr>   <tr><td align="center">
      TOTAL OPEN
   </td><td align="center">
      6154
   </td><td align="center">
      5989
   </td><td align="center">
      -165
   </td></tr>
   <tr><td align="center">
       TOTAL
   </td><td align="center">
      39255
   </td><td align="center">
      39207
   </td><td align="center">
      -48
   </td></tr>
</table>
</center></p>
<br /><br />
<center><b>AppDB Application Status Changes</b></center>
<p><i>*Disclaimer: These lists of changes are automatically  generated by information entered into the AppDB.
These results are subject to the opinions of the users submitting application reviews.
The Wine community does not guarantee that even though an application may be upgraded to 'Gold' or 'Platinum' in this list, that you
will have the same experience and would provide a similar rating.</i></p>
<div align="center">
   <b><u>Updates by App Maintainers</u></b><br /><br />
    <table width="80%" border="1" bordercolor="#222222" cellspacing="0" cellpadding="3">
      <tr>
        <td><b>Application</b></td>
        <td width="140"><b>Old Status/Version</b></td>
        <td width="140"><b>New Status/Version</b></td>
        <td width="20" align="center"><b>Change</b></td>
      </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=30495">Blade &amp; Soul PlayBNS.com alpha</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.50)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=2344">Desperados: Wanted Dead or Alive 1.x</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.18)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=806">The Incredible Machine 3</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.6.2)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=9348">Star Wars Jedi Knight: Dark Forces II 1.01</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (0.9.53)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32523">The Bat! 7.x</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.50)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.51)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32045">FL Studio 12.0</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.44)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.50)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=20974">4Star Wars Jedi Knight: Dark Forces II JK 1.01 Uno...</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.3.0)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.51)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32206">Wrye Bash 3xx</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.43)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td colspan="3">
                Total Change
             </td><td align="center">
               <div style="color: #000000;">+5</div>
             </td>
           </tr>
        </table>  <br />   <b><u> Updates by the Public </u></b> <br /><br />
   <table width="80%" border="1" bordercolor="#222222" cellspacing="0" cellpadding="3">
      <tr>
        <td><b>Application</b></td>
        <td width="140"><b>Old Status/Version</b></td>
        <td width="140"><b>New Status/Version</b></td>
        <td width="20"><b>Change</b></td>
      </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=27093">Adobe Reader 11.x</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.6.2)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=23220">Anomaly: Warzone Earth Steam</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.5.15)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=7891">Bus Driver Demo</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (0.9.52)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.50)
             </td><td align="center">
                <div style="color: #000000;">+4</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=14007">Cinema Tycoon 2 1.0</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.1.5)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.6.2)
             </td><td align="center">
                <div style="color: #000000;">+4</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=28189">Fur Fighters 1.2</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.28)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.50)
             </td><td align="center">
                <div style="color: #000000;">+4</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=16336">1Galactic Civilizations II: Dread Lords Ultimate E...</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.22)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=13233">Harry Potter and the Prisoner of Azkaban 1.0</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.5.10)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.6.2)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31665">LIfe Is Strange Steam</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.6.2)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31866">Ori and the Blind Forest (Steam) Steam Version</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.41)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=2505">Paint Shop Pro 9.x</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.5.2)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.6.2)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=8370">Ragnarok Online Private Server Clients</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.50)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=3851">Railroad Tycoon 3 CD</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.5.31)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.6.2)
             </td><td align="center">
                <div style="color: #000000;">+4</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=9594">Touhou 6 ~ Embodiment of Scarlet Devil 1.02</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.6.1)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=28814">papers,please GoG</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.4.1)
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=30436">Aerosoft Launcher 1.2.0.3</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.6.2)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.50)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=30358">Age Of Wushu 0.0.1.117</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.18)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.50)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=22230">Bully: Scholarship Edition 1.154</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.3.9)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=19257">Divine Divinity II 1.03</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.3.8)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.50)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=18130">Enzai 1.00</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.3.31)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=22627">Magicka Steam</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.35)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=19203">Star Trek Online Current Version</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.50)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=30781">Unholy Heights 1.0</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.22)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=2480">WinCupl 5.30</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.6.2)
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.46)
             </td><td align="center">
                <div style="color: #000000;">+3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=7898">Aliens Versus Predator 2 1.0.9.6</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.0)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.48)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=11909">Avernum 4 1.0.1</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (0.9.59)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=257">Black &amp; White 1.0</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.44)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=1639">Bugs Bunny Lost in Time 1.0</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (0.9.30)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.6.2)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=21692">Fallout: New Vegas 1.x</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.44)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.50)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32545">LINE (Powered by Naver) 4.1.x.x</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.49)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.51)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=5038">Monster Truck Madness 2 2.00.41</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.2.2)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.46)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=12409">Origin 8</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.1.37)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.50)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=30497">Pac-Man Championship Edition DX+ 1.0.4.1</a>
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.43)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.50)
             </td><td align="center">
                <div style="color: #000000;">+1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=21882">Rollercoaster Tycoon 1.x</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.6.1)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.6.2)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=5618">Silent Hunter III 1.4b-EMEA</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.12)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.50)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=3859">WinISIS 1.5 build 3</a>
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.5.21)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.51)
             </td><td align="center">
                <div style="color: #000000;">+2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=7083">Zoo Tycoon 2 Endangered Species Expansion</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.2.2)
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.7.51)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=13642">Etherlords 1.07</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.0)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.50)
             </td><td align="center">
                <div style="color: #990000;">-3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=32431">HeidiSQL 9.0.0.x</a>
             </td><td background="{$root}/images/wwn_platinumbg.gif">
               Platinum (1.7.42)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.51)
             </td><td align="center">
                <div style="color: #990000;">-3</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=6224">Need for Speed: Carbon 1.2</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.3.3)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.6.2)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=23721">PDF-XChange PDF Viewer 2.5.x</a>
             </td><td background="{$root}/images/wwn_silverbg.gif">
               Silver (1.6.1)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.6.2)
             </td><td align="center">
                <div style="color: #990000;">-1</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=7283">Startopia 1.02</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.5.29)
             </td><td background="{$root}/images/wwn_bronzebg.gif">
               Bronze (1.7.51)
             </td><td align="center">
                <div style="color: #990000;">-2</div>
             </td>
           </tr>           <tr>
             <td>
                <a href="//appdb.winehq.org/objectManager.php?sClass=version&amp;iId=31813">The Elder Scrolls Online 1.5.7.1103452</a>
             </td><td background="{$root}/images/wwn_goldbg.gif">
               Gold (1.7.43)
             </td><td background="{$root}/images/wwn_garbagebg.gif">
               Garbage (1.7.50)
             </td><td align="center">
                <div style="color: #990000;">-3</div>
             </td>
           </tr>           <tr>
             <td colspan="3">
                Total Change
             </td><td align="center">
               <div style="color: #000000;">+42</div>
             </td>
           </tr>
        </table></div>
</section></kc>
